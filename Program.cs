using System;

namespace Calculadora
{

    class Solicitar
    {
        public double monto;
        public double plazo;
        public double tasa;

    }
    class Program
    {
    
        static void TablaAmortizacion(ref double tasa,ref double cuotas, ref double resta, ref double plazo)
        {
            //declaracion de arrays
            double[] balance = new double[100];
            double[] capital = new double[100];
            double[] interes = new double[100];
            tasa = tasa / 100;
           

            Console.WriteLine("{0,3}{1,12:N2}{2,12:N2}{3,12:N2}{4,12:N2}", "Nº", "Cuota", "Capital", "Interés", "Saldo");
            Console.WriteLine("{0,3}{1,12}{2,12}{3,12}{4,12}", "---", "----------", "----------", "----------", "----------");
            for (int k = 0; k < plazo; k++)
            {
                 interes[k] = resta * tasa / 12;
                resta += interes[k] - cuotas;

                 capital[k] = cuotas - interes[k]; //valores a imprimir
                 balance[k] = resta;


                Console.WriteLine("{0,3}{1,12:N2}{2,12:N2}{3,12:N2}{4,12:N2}", k + 1, cuotas, capital[k], interes[k], balance[k]);
            }
        }

        static double Calcular(double monto, double plazo, double tasa)
        {
            //Calculo de la cuota
            double k, cuota;
            tasa = (tasa / 1200);
            k = Math.Pow((1 + tasa), plazo);
            cuota = tasa * monto * k / (k - 1);

            return cuota;
            
        }
        static void Main(string[] args)
        {
            double m, t, p; //estas variables hacen referencia a m = monto, t= tasa, p= plazo.
            double cuotasG; //en esta variable se almacena la funcion Calcular

            //llamada de los atributos de la clase Solicitar
             Solicitar sol = new Solicitar();
            Console.WriteLine("CALCULADORA DE PRÉSTAMOS ");


            Console.WriteLine("Ingrese el monto");
            sol.monto = double.Parse(Console.ReadLine()); 
            m = sol.monto;
            Console.WriteLine("Ingrese el plazo");
            sol.plazo = double.Parse(Console.ReadLine());
            p = sol.plazo;
            Console.WriteLine("Ingrese la tasa de interes anual");
            sol.tasa = double.Parse(Console.ReadLine());
            t = sol.tasa;

            cuotasG = Calcular(m, p, t);

            Console.WriteLine();
            Console.WriteLine("El monto de la cuota es: {0}", cuotasG );
            Console.WriteLine();

            TablaAmortizacion(ref t,ref cuotasG, ref m, ref p);

            Console.ReadKey();

        }
    }
}
